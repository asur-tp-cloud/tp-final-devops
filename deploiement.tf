# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "> 1.48.0"
    }
  }
}

# Configure the OpenStack Provider
provider "openstack" {
  user_name   = "${var.OS_USERNAME}"
#  tenant_name = $OS_TENANT_NAME
  password    = "${var.OS_PASSWORD}"
  auth_url    = "${var.OS_AUTH_URL}"
#  region      = $OS_REGION_NAME
}



#    Network

    resource "openstack_networking_network_v2" "network" {
    count			 = "${length(var.network)}"
    name           = "${lookup(var.network[count.index],"name")}"
    admin_state_up = "${lookup(var.network[count.index],"admin_state_up")}"
    region         = "${lookup(var.network[count.index],"region")? 1 : 0}"
    }

#    Subnet

    resource "openstack_networking_subnet_v2" "subnet" {  
    count		 = "${length(var.subnet)}"  
    name       = "${lookup(var.subnet[count.index],"name")}"  
    cidr       = "${lookup(var.subnet[count.index],"cidr")}"  
    network_id = "${element(openstack_networking_network_v2.network.*.id,lookup(var.subnet[count.index],network_id))}"  
    ip_version = "${lookup(var.subnet[count.index],"ip_version")}"  
    region     = "${lookup(var.network[count.index],"region")}"  
    }

#    Router

    resource "openstack_networking_router_v2" "router" {  
    count				  = "${length(var.router)}"  
    name                = "${lookup(var.router,"name")}"  
    admin_state_up      = "${lookip(var.router,"admin_state_up")}"  
    external_network_id = "${element(openstack_networking_network_v2.network.*.id,lookup(var.router[count.index],"network_id"))}"  
    region              = "${lookup(var.network,"region")}"  
    }

#    Router Interface

    resource "openstack_networking_router_interface_v2" "router_interface" {  
    count     = "${ "${length(var.router)}" == "0" ? "0" : "${lenght(var.subnet)}" }"  
    router_id = "${element(openstack_networking_router_v2.routeur.*.id,lookup(var.router_interface[count.index],"routeur_id"))}"  
    subnet_id = "${element(openstack_networking_subnet_v2.subnet.*.id,lookup(var.router_interface[count.index],"subnet_id"))}"  
    }

#    Floating IP

    resource "openstack_networking_floatingip_v2" "floating_ip" {  
    count  = "${lenght(var.floating_ip)}"  
    pool   = "${lookup(var.floating_ip[count.index],"pool")}"  
    region = "${lookup(var.network,"region")}"  
    }


