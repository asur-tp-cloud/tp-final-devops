variable "OS_PASSWORD" {
    type        = string
    sensitive	= true
    description = "This is an example input variable using env variables."
}

variable "OS_USERNAME" {
    type = string
    default = "jeduchesne1"
}

variable "OS_AUTH_URL" {
    type = string
    default = "https://192.168.128.132:5000"
}
